import React from "react";

import { MDashboard } from '../pages/m-dashboard/m-dashboard'
import { LDashboard } from '../pages/l-dashboard/l-dashboard'


export const MainHome = () => {
 {
    return (

        <div className="layout-main" style={{height: '100%', position: 'absolute'}}>
            <MDashboard/>
            <LDashboard/>
        </div>
      );
  }
}

