import React from "react";
import { shallow } from "enzyme";
import MDashboard from "./m-dashboard";

describe("MDashboard", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<MDashboard />);
    expect(wrapper).toMatchSnapshot();
  });
});
