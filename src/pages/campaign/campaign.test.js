import React from "react";
import { shallow } from "enzyme";
import Campaign from "./campaign";

describe("Campaign", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<Campaign />);
    expect(wrapper).toMatchSnapshot();
  });
});
