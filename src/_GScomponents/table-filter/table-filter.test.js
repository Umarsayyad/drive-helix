import React from "react";
import { shallow } from "enzyme";
import TableFilter from "./table-filter";

describe("TableFilter", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<TableFilter />);
    expect(wrapper).toMatchSnapshot();
  });
});
