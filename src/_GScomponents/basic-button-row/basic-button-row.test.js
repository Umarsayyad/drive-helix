import React from "react";
import { shallow } from "enzyme";
import BasicButtonRow from "./basic-button-row";

describe("BasicButtonRow", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<BasicButtonRow />);
    expect(wrapper).toMatchSnapshot();
  });
});
