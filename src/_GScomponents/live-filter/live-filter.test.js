import React from "react";
import { shallow } from "enzyme";
import LiveFilter from "./live-filter";

describe("LiveFilter", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<LiveFilter />);
    expect(wrapper).toMatchSnapshot();
  });
});
