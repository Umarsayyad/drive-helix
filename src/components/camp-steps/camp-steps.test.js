import React from "react";
import { shallow } from "enzyme";
import CampSteps from "./camp-steps";

describe("CampSteps", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<CampSteps />);
    expect(wrapper).toMatchSnapshot();
  });
});
