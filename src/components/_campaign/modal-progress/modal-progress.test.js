import React from "react";
import { shallow } from "enzyme";
import ModalProgress from "./modal-progress";

describe("ModalProgress", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<ModalProgress />);
    expect(wrapper).toMatchSnapshot();
  });
});
