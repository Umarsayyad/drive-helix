import React, { Component } from "react";
import { CampSteps } from '../camp-steps/camp-steps';
import { Button } from 'primereact/button';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch
} from "react-router-dom";

import { ModalProgress } from './modal-progress/modal-progress'
import { ModalHeader } from './modal-header/modal-header'
import { ModalBase } from './modal-base/modal-base'
import { ModalFooter } from './modal-footer/modal-footer'

export const ModalRouter = () => {
{
    let { path, url } = useRouteMatch();
    return (
        <div className="" >
        <div className="main-sec p-col-12 p-mr-2 p-as-stretch">
            <div>
            <Switch>
            <Route
                render={({ match: { url } }) => (
                <>
                <Route exact component={ModalBase} path={`${url}/`}>
                    <ModalBase/>
                </Route>
                <Route path={`${url}/1`} component={ModalBase} />
            </> 
            )}
            />
            </Switch>
            </div>
            <div>
                <ModalFooter/>
            </div>
        </div>
        </div>
        );
    }
}

