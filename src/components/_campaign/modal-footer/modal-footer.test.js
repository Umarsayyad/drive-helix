import React from "react";
import { shallow } from "enzyme";
import ModalFooter from "./modal-footer";

describe("ModalFooter", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<ModalFooter />);
    expect(wrapper).toMatchSnapshot();
  });
});
