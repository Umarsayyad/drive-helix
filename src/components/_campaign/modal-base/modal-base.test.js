import React from "react";
import { shallow } from "enzyme";
import ModalBase from "./modal-base";

describe("ModalBase", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<ModalBase />);
    expect(wrapper).toMatchSnapshot();
  });
});
