import React from "react";
import { shallow } from "enzyme";
import ModalHeader from "./modal-header";

describe("ModalHeader", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<ModalHeader />);
    expect(wrapper).toMatchSnapshot();
  });
});
