import React from "react";
import { shallow } from "enzyme";
import Dgraph from "./dgraph";

describe("Dgraph", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<Dgraph />);
    expect(wrapper).toMatchSnapshot();
  });
});
