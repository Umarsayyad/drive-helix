import React, { useState, useEffect, useRef } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { MultiSelect } from 'primereact/multiselect';
import { CustomerService } from '../../../service/CustomerService';
import { Button } from 'primereact/button';
import { Modal } from '../../_campaign/modal/modal';
import { ModalBase } from '../../_campaign/modal-base/modal-base'


import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams,
    useRouteMatch
  } from "react-router-dom";
  
// style
import './inventory.scss'
export const Inventory = () => {

const [isShowing, setIsShowing] = useState(false);

function toggle() {
    setIsShowing(!isShowing);
}

  const [loading, setLoading] = useState(false);
  const [totalRecords, setTotalRecords] = useState(0);
  const [customers, setCustomers] = useState(null);
  const [selectedRepresentative, setSelectedRepresentative] = useState(null);
  const [lazyParams, setLazyParams] = useState({
      first: 0,
      rows: 10,
      page: 1,
  });

  const dt = useRef(null);

  const representatives = [
      { name: "Amy Elsner", image: 'amyelsner.png' },
      { name: "Anna Fali", image: 'annafali.png' },
      { name: "Asiya Javayant", image: 'asiyajavayant.png' },
      { name: "Bernardo Dominic", image: 'bernardodominic.png' },
      { name: "Elwin Sharvill", image: 'elwinsharvill.png' },
      { name: "Ioni Bowcher", image: 'ionibowcher.png' },
      { name: "Ivan Magalhaes", image: 'ivanmagalhaes.png' },
      { name: "Onyama Limba", image: 'onyamalimba.png' },
      { name: "Stephen Shaw", image: 'stephenshaw.png' },
      { name: "XuXue Feng", image: 'xuxuefeng.png' }
  ];

  const customerService = new CustomerService();

  let  color = ['red', 'blue', 'green', 'yellow']

  var divStyle = {
    color: color +'!important',
  };

  let loadLazyTimeout = null;

  useEffect(() => {
      loadLazyData();
  },[lazyParams]) // eslint-disable-line react-hooks/exhaustive-deps

  const loadLazyData = () => {
      setLoading(true);

      if (loadLazyTimeout) {
          clearTimeout(loadLazyTimeout);
      }

      //imitate delay of a backend call
      loadLazyTimeout = setTimeout(() => {
          customerService.getCustomers({lazyEvent: JSON.stringify(lazyParams)}).then(data => {
              setTotalRecords(data.totalRecords);
              setCustomers(data.customers);
              setLoading(false);
          });
      }, Math.random() * 1000 + 250);
  }

  const onPage = (event) => {
      let _lazyParams = { ...lazyParams, ...event };
      setLazyParams(_lazyParams);
  }

  const onSort = (event) => {
      let _lazyParams = { ...lazyParams, ...event };
      setLazyParams(_lazyParams);
  }

  const onFilter = (event) => {
      let _lazyParams = { ...lazyParams, ...event };
      _lazyParams['first'] = 0;
      setLazyParams(_lazyParams);
  }

  const representativeBodyTemplate = (rowData) => {
      return (
          <React.Fragment>
         
            {/* break */}
            <div className="img-org">
              <img className="img-thumb" alt={rowData.representative.name} src={`showcase/demo/images/avatar/${rowData.representative.image}`} onError={(e) => e.target.src = 'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png'} style={{ verticalAlign: 'middle' }} />
              </div>
              <div className="img-text">
                <h2 className="img-text-cont">{rowData.representative.name}</h2>
                <p className="img-text-cont">
                 {rowData.representative.name}
                </p>
                <p className="img-text-cont">
                {rowData.representative.name}
                </p>
              </div>
          </React.Fragment>
      );
  }

  const representativesItemTemplate = (option) => {
      return (
          <div className="p-multiselect-representative-option">
              <img alt={option.name} src={`showcase/demo/images/avatar/${option.image}`} onError={(e) => e.target.src = 'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png'} width={32} style={{ verticalAlign: 'middle' }} />
              <span className="image-text">{option.name}</span>
          </div>
      );
  }

  const onRepresentativesChange = (e) => {
      dt.current.filter(e.value, 'representative.name', 'in');
      setSelectedRepresentative(e.value);
  }

  const countryBodyTemplate = (rowData) => {
      return (
            isShowing,toggle,
          <React.Fragment>
              
              {/* <img alt="flag" src="showcase/demo/images/flag_placeholder.png" onError={(e) => e.target.src = 'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png'} className={`flag flag-${rowData.country.code}`} width={30} />
              <span className="image-text">{rowData.country.name}</span> */}
              <Button label='Promote' onClick={toggle}/>
              {/* <button style={divStyle} className="p-button p-component"></button> */}
          </React.Fragment>
      );
  }

  const representativeFilter = <MultiSelect value={selectedRepresentative} options={representatives} filter itemTemplate={representativesItemTemplate} onChange={onRepresentativesChange} optionLabel="name" optionValue="name" placeholder="All" className="p-column-filter" />;
  let { path, url } = useRouteMatch();

  return (
      <div style={{height: '100%'}}>
            <Switch>
            <Route
                render={({ match: { url } }) => (
                <>
                <Route  component={ModalBase} path={`${url}/`}>
                    <Modal
                        isShowing={isShowing}
                        hide={toggle}
                    />
                </Route>
                <Route path={`${url}/1`} component={ModalBase} />
            </> 
            )}
            />
            </Switch>
          <div className="content-section implementation">
              <div className="card stretch">
                  <DataTable ref={dt} value={customers} lazy
                      paginator first={lazyParams.first} rows={10} totalRecords={totalRecords} onPage={onPage}
                      onSort={onSort} sortField={lazyParams.sortField} sortOrder={lazyParams.sortOrder}
                      onFilter={onFilter} filters={lazyParams.filters} loading={loading}>
                      {/* <Column field="name" header="Name" sortable filter filterPlaceholder="Search by name" />
                      <Column field="country.name" sortable header="Country" filterField="country.name" body={countryBodyTemplate} filter filterPlaceholder="Search by country" filterMatchMode="contains" /> */}
                      <Column field="representative.name" body={representativeBodyTemplate} filter filterElement={representativeFilter} />
                      <Column field="representative.name"  body={countryBodyTemplate} filter filterElement={representativeFilter} />
                      {/* <Column field="company" sortable filter header="Company" filterPlaceholder="Search by company" /> */}
                  </DataTable>
              </div>
          </div>
      </div>
  );

}
               

