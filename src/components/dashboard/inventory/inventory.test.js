import React from "react";
import { shallow } from "enzyme";
import Inventory from "./inventory";

describe("Inventory", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<Inventory />);
    expect(wrapper).toMatchSnapshot();
  });
});
