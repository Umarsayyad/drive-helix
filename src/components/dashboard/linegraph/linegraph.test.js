import React from "react";
import { shallow } from "enzyme";
import Linegraph from "./linegraph";

describe("Linegraph", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<Linegraph />);
    expect(wrapper).toMatchSnapshot();
  });
});
