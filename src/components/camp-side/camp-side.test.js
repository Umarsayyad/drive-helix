import React from "react";
import { shallow } from "enzyme";
import CampSide from "./camp-side";

describe("CampSide", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<CampSide />);
    expect(wrapper).toMatchSnapshot();
  });
});
