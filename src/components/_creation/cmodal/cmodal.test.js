import React from "react";
import { shallow } from "enzyme";
import Cmodal from "./cmodal";

describe("Cmodal", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<Cmodal />);
    expect(wrapper).toMatchSnapshot();
  });
});
