import React from "react";
import { useHistory } from "react-router-dom";

export const CModal = () => {
  const history = useHistory();

  const closeModal = e => {
    e.stopPropagation();
    history.goBack();
  };

  React.useEffect(() => {
    document.body.classList.add("overflow-hidden");

    return () => {
      document.body.classList.remove("overflow-hidden");
    };
  }, []);

  return (
    <div className="absolute inset-0 bg-black bg-opacity-75 w-full h-screen z-10 flex items-center justify-center">
      <span
        className="inline-block absolute top-0 right-0 mr-4 mt-4 cursor-pointer"
        onClick={closeModal}
      >
    
      </span>
      <h1>hello</h1>
    </div>
  );
};

