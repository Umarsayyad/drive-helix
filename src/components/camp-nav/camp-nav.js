import React, { Component } from "react";
import { Accordion, AccordionTab } from 'primereact/accordion';
import { Button } from 'primereact/button';
// import { Chart } from 'primereact/chart';
import './camp-nav.scss'

export const CampNav = () => {
  const chartData = {
    // labels: ['A', 'B', 'C'],
    datasets: [
        {
            data: [200, 50, 100],
            backgroundColor: [
                "BLUE",
                "GREEN",
                "#FFCE56"
            ],
            hoverBackgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ]
        }]
};

const lightOptions = {
    legend: {
        labels: {
            fontColor: '#495057'
        }
    }
};
  {
     return (
       <div className="main-layout-sidenav" >
         {/* <div className="p-col-11">
           <Chart type="doughnut" data={chartData} options={lightOptions} />
         </div> */}
        <Accordion className="camp-acc">
        <AccordionTab header="Header I">
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
        </AccordionTab>
        <AccordionTab header="Header II">
        <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
        </AccordionTab>
        <AccordionTab header="Header III">
        <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
        </AccordionTab>
        <AccordionTab header="Header III">
        <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
        </AccordionTab>
        <AccordionTab header="Header III">
        <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
          <Button label="Secondary" className="but-spc p-button-secondary p-button-text" icon="pi pi-check" iconPos="left" />
        </AccordionTab>
      </Accordion>
    </div>
       );
   }
 }

