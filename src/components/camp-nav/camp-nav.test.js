import React from "react";
import { shallow } from "enzyme";
import CampNav from "./camp-nav";

describe("CampNav", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<CampNav />);
    expect(wrapper).toMatchSnapshot();
  });
});
