import React from "react";
import { shallow } from "enzyme";
import CampMain from "./camp-main";

describe("CampMain", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<CampMain />);
    expect(wrapper).toMatchSnapshot();
  });
});
