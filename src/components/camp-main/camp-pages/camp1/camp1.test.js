import React from "react";
import { shallow } from "enzyme";
import Camp1 from "./camp1";

describe("Camp1", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<Camp1 />);
    expect(wrapper).toMatchSnapshot();
  });
});
