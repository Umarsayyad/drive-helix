import React from "react";
import { shallow } from "enzyme";
import Camp2 from "./camp2";

describe("Camp2", () => {
  test("matches snapshot", () => {
    const wrapper = shallow(<Camp2 />);
    expect(wrapper).toMatchSnapshot();
  });
});
